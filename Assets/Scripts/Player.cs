﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
   
    public int velMovimiento = 10;
    public int velRotacion = 10;
    public int jumpForce = 10;
    bool canJump;

    // Update is called once per frame
    void FixedUpdate()
    {
        //Esta parte es movimiento, rotacion y salto del jugador
        float movimiento = Input.GetAxis("Vertical") * velMovimiento * Time.deltaTime;
        float rotacion = Input.GetAxis("Horizontal") * velRotacion * Time.deltaTime;

        transform.Translate(0, 0, movimiento);
        transform.Rotate(0, rotacion, 0);
    }
    void Update()
    {
        
    }
    //Este es un comprobador de salto
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Terreno") || other.gameObject.CompareTag("Plataforma"))
        {
            canJump = false;
        }
    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("Terreno") || other.gameObject.CompareTag("Plataforma"))
        {
            canJump = true;
        }
    }
}
